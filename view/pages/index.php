<header>
    <div class="container">
        <div class="pull-left logo">
            <a href="javascript:void(0)">
                <img src="public/image/logo.png" alt="logo">
            </a>
        </div>

        <div class="pull-right nav-inline">
            <nav>
                <ul>
                    <li><a href="javascript:void(0)">home</a></li>
                    <li><a href="javascript:void(0)">new</a></li>
                    <li><a href="javascript:void(0)">women</a></li>
                    <li><a href="javascript:void(0)">men</a></li>
                    <li><a href="javascript:void(0)">blog</a></li>
                    <li><a href="?controller=pages&action=create">Create Product</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>

<!--end header html-->


<section class="brecrumbs">
    <div class="container">
        <span>
            <a href="" class="brecrumbs__home">
                <i class="fa fa-home" aria-hidden="true"></i>
            </a><span class="brecrumbs__chevron"><i class="fa fa-chevron-right" aria-hidden="true"></i></span><a href="">Shop</a>
        </span>
    </div>
</section>

<!--end section brecrumbs-->

<section class="category">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3 left-col">
                <p class="filter-title">categories</p>

                <div class="multi-menu-category">
                   <?php echo $menuCateLevel ?>
                </div>

                <!--end html multi menu-->

                <p class="filter-title">Brand</p>
                <div class="brand-filter filter-group">
                    <?php foreach($brand as $brand): ?>
                        <div class="list-group-item checkbox">
                            <label><input type="checkbox" class="filter_selector brand" value="<?php echo $brand['brand']; ?>"  > <?php echo $brand['brand']; ?></label>
                        </div>
                    <?php endforeach ?>
                </div>
                <!--end html category block--->

                <p class="filter-title">Color</p>
                <div class="brand-filter filter-group">
                    <?php foreach($color as $color): ?>
                        <div class="list-group-item checkbox">
                            <label><input type="checkbox" class="filter_selector color" value="<?php echo $color['id']; ?>"  > <?php echo $color['value']; ?></label>
                        </div>
                    <?php endforeach ?>
                </div>
                <!--end html color block--->

                <p class="filter-title">Size</p>
                <div class="brand-filter filter-group">
                    <?php foreach($size as $size): ?>
                        <div class="list-group-item checkbox">
                            <label><input type="checkbox" class="filter_selector size" value="<?php echo $size['id']; ?>"  > <?php echo $size['value']; ?></label>
                        </div>
                    <?php endforeach ?>
                </div>
                <!--end html size block--->


            </div>

            <div class="col-xs-12 col-sm-9 col-md-9 right-col container filter-product">
                <?php foreach(array_chunk($productData, 3) as $productData): ?>
                <div class="row row-product">
                    <?php foreach($productData as $product): ?>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-product">
                        <div class="infoProduct text-center">
                            <div class="infoProduct__img">
                                <a href="">
                                    <img src="public/image/<?php echo $product['image'] ?>" alt="anh san pham">
                                </a>
                            </div>
                            <div class="infoProduct__name-product">
                                <?php echo $product['name'] ?>
                            </div>

                            <div class="infoProduct__price">
                                <?php echo $product['price'] ?>
                            </div>
                            <div class="infoProduct__add-to-cart">
                                <button class="btn">Add to cart</button>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
                <?php endforeach ?>

                <div class="pagination-group">
                    <?php echo $pagination ?>
                </div>
            </div>
        </div>
    </div>
    <script src="public/js/index.js"></script>
</section>

<section class="about-us">
    <div class="container">
        <div class="row about-us-content">
            <div class="col-xs-12 col-sm-3 col-md-3">
                <p class="about-title">about us</p>

                <ul class="about-info">
                    <li>Lorem ipsum dolor sit ament , conse ctetur adipiscing</li>
                    <li><span><i class="fa fa-clock-o" aria-hidden="true"></i></span>9.00 - 18.00 Monday - Friday</li>
                    <li><span><i class="fa fa-phone" aria-hidden="true"></i></span> 08.5555.9172</li>
                    <li><span><i class="fa fa-envelope-o" aria-hidden="true"></i></span> phamtiendzbk@gmail.com</li>
                </ul>
            </div>

            <div class="col-xs-12 col-sm-3 col-md-3">
                <p class="about-title">information</p>

                <ul class="about-info">
                    <li><a href="">About Us</a></li>
                    <li><a href="">Delivery Infomation</a></li>
                    <li><a href="">Privacy & Policy</a></li>
                    <li><a href="">Terms & Conditions</a></li>
                    <li><a href="">Manufactures</a></li>
                </ul>
            </div>

            <div class="col-xs-12 col-sm-3 col-md-3">
                <p class="about-title">company</p>

                <ul class="about-info">
                    <li><a href="">Careers</a></li>
                    <li><a href="">Affiliate Program</a></li>
                    <li><a href="">Social Responsibility</a></li>
                    <li><a href="">Business With Us</a></li>
                    <li><a href="">Our Team</a></li>
                </ul>
            </div>


            <div class="col-xs-12 col-sm-3 col-md-3">
                <p class="about-title">instagram</p>

                <div class="row ins-img-row">
                    <div class="col-xs-12 col-sm-4 ins-img-col">
                        <img src="public/image/p1.jpg" alt="">
                    </div>
                    <div class="col-xs-12 col-sm-4 ins-img-col">
                        <img src="public/image/p2.jpg" alt="">
                    </div>
                    <div class="col-xs-12 col-sm-4 ins-img-col">
                        <img src="public/image/p3.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end section about us-->

<footer>
    <div class="container">
            <div class="footer-content">
            <div class="pull-left copyright">
                @coypright <?php echo Date('Y') ?> by <span class="author">PhamDucTienDZ</span>. All Rights Reseved
            </div>

            <div class="pull-right">
                <div class="footer-payment">
                    <div class="footer-payment__item">
                        <img src="public/image/payment1.png" alt="">
                    </div>
                    <div class="footer-payment__item">
                        <img src="public/image/payment2.png" alt="">
                    </div>
                    <div class="footer-payment__item">
                        <img src="public/image/payment2.png" alt="">
                    </div>
                    <div class="footer-payment__item">
                        <img src="public/image/payment1.png" alt="">
                    </div>
                    <div class="footer-payment__item">
                        <img src="public/image/payment2.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
