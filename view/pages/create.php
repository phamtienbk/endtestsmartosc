<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>create product page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="public/css/style.css">
</head>
<body class="bg-gray">
    <div class="container create-page">
        <div class="row justify-content-sm-center">
            <div class="col-xs-12 col-sm-6 col-md-6 create-page__content">
                <div class="create-head text-center">
                    <img src="public/image/logo.png" alt="logo image">
                    <h3>Add product</h3>
                </div>

                <div class="create-form">
                    <form action="index.php?controller=pages&action=store"method="POST" enctype="multipart/form-data" id="create-product-form">
                        <div class="form-group">
                            <label for="">Name product</label>
                            <input type="text" class="form-control" placeholder="" name="nameProduct">
                        </div>

                        <div class="form-group">
                            <label for="">Slug</label>
                            <input type="text" class="form-control" placeholder="" name="slugProduct">
                        </div>

                        <div class="form-group">
                            <label for="">Price product</label>
                            <input type="text" class="form-control" placeholder="" name="priceProduct">
                        </div>

                        <div class="form-group">
                            <label for="">Image</label>
                            <input type="file" class="form-control" placeholder=""  id="imageProduct" name="imageProduct" multiple>
                        </div>

                        <div class="form-group">
                            <label for="">Brand</label>
                            <input type="text" class="form-control" placeholder="" name="brandProduct">
                        </div>

                        <div class="form-group multi-check-group">
                            <label for="">Select category</label>
                            <div class="multi-select-cate-button">check category product you want</div>
                            <div class="multi-select-cate">
                                <?php echo $menuCateLevel ?>
                            </div>
                        </div>

                        <div class="form-group multi-check-group">
                            <p><label for="">Color</label></p>
                            <?php foreach($color as $color): ?>
                                <label class="checkbox-inline">
                                    <input type="checkbox" class="color" value="<?php echo $color['id'] ?>" name="colorProduct[]"> <?php echo $color['value'] ?>
                                </label>
                            <?php endforeach ?>
                        </div>


                        <div class="form-group multi-check-group">
                            <p><label for="">Size</label></p>
                            <?php foreach($size as $size): ?>
                                <label class="checkbox-inline">
                                    <input type="checkbox" value="<?php echo $size['id'] ?>" name="sizeProduct[]"> <?php echo $size['value'] ?>
                                </label>
                            <?php endforeach ?>
                        </div>

                        <div class="form-group multi-check-group">
                            <p><label for="">Status</label></p>
                            <div class="disable pull-left">
                                <label class="radio-inline">
                                    <input name="status_type" id="input-status-type" value="0" type="radio" />disable
                                </label>
                            </div>

                            <div class="disable pull-right">
                                <label class="radio-inline">
                                    <input name="status_type" id="input-status-type" value="1" type="radio" />enable
                                </label>
                            </div>
                        </div>

                        <button class="btn btn-login" type="submit" name="createProduct" data-toggle="modal" data-target="#myModal">Create product</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--notification modal for validate server-->
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Notification</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="notification">OPP! What wrong with form add product</div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger close-model" data-dismiss="modal">Close</button>
            </div>

            </div>
        </div>
    </div>
    <script src="public/js/create.js"></script>
</body>
</html>
