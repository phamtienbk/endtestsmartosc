<?php
    $controllers = array(
        'pages' => ['index', 'error', 'fetchProduct', 'create', 'store']
    );

    if (!array_key_exists($controller, $controllers) || !in_array($action, $controllers[$controller])) {
        $controller = 'pages';
        $action = 'error';
    }

    include('controller/' . $controller . '_controller.php');
    $nameClassController = str_replace('_', '', ucwords($controller, '_')) . 'Controller';
    $controller = new $nameClassController;
    $controller->$action();

