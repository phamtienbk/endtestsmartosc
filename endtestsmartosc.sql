-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 14, 2019 at 02:22 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `endtestsmartosc`
--

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE attributes (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `name`, `value`) VALUES
(1, 'color', 'red'),
(2, 'color', 'blue'),
(3, 'color', 'lightgray'),
(4, 'size', 'M'),
(5, 'size', 'L'),
(6, 'size', 'XL'),
(7, 'color', 'gray'),
(8, 'size', 'S');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parentID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `slug`, `parentID`) VALUES
(1, 'man', 'man', 0),
(2, 'women', 'women', 0),
(3, 'kids', 'kids', 0),
(4, 'jewellery', 'jewellery', 0),
(5, 'jeans', 'jeans', 1),
(6, 'shoes', 'shoes', 1),
(7, 'shirts', 'shirts', 1),
(8, 'dress', 'dress', 2);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `slug`, `price`, `image`, `brand`, `status`) VALUES
(1, 'jean 1', 'jean-1', 200, 'p1.jpg', 'kollar', 1),
(2, 'jean 2', 'jean-2', 300, 'p1.jpg', 'tume machines', 1),
(3, 'jean 3', 'jean-3', 400, 'p1.jpg', 'kollar', 1),
(4, 'shoe 1', 'shoe-1', 250, 'p1.jpg', 'Nike', 1),
(5, 'shoe 2', 'shoe-2', 300, 'p1.jpg', 'Nike', 1),
(6, 'jewelly 1', 'jewelly-1', 900, 'p1.jpg', 'Sai Gon Hip Hop jewelly', 1),
(8, 'jean 4', 'jean-4', 333, 'p1.jpg', 'kollar', 1),
(11, 'jean 6', 'jean-6', 333, 'p1.jpg', 'kollar', 1),
(12, 't shirt 1', 'shirt-1', 444, 'p1.jpg', 'viet tien', 1),
(13, 't shirt 2', 'shirt-2', 555, 'p1.jpg', 'viet tien', 1),
(14, 'Tshirt3', 'shirt-3', 455, 'p1.jpg', 'viet tien', 0),
(15, 'shoe1', 'shoe-1', 988, 'p1.jpg', 'addidas', 1),
(16, 'chealsea boot', 'chealse-boot', 987, 'p1.jpg', 'kollar', 1),
(17, 'adddaa', 'aaddada', 333, 'p1.jpg', 'kollar', 1),
(18, 'dadad', 'djdjdhdd', 444, 'p1.jpg', 'kollar', 1),
(19, 'ddeueye', 'dhhdh', 373, 'p1.jpg', 'viet tien', 1),
(20, 'dydydyaa', 'dhdhdhd', 444, 'p1.jpg', 'kollar', 1),
(21, 'eueuyeyey', 'eyeyeiei', 38, 'p1.jpg', 'kollar', 1),
(22, 'eyeyeteddh', 'yeyey77', 344, 'p1.jpg', 'kollar', 1),
(23, 'dhdhdh', 'dhahei', 877, 'p1.jpg', 'kollar', 1),
(24, 'dddhdh', 'dhdhgad', 3636, 'p1.jpg', 'kollar', 1),
(25, 'daaddd', 'dasddd', 34646, 'p2.jpg', 'kollar', 1),
(26, 'ddhayah', 'dhahdu', 6645, 'Đang-1565234465.jpg', 'kollar', 1),
(27, 'dhdhgagaaa', 'dasddd', 333, 'p2.jpg', 'kollar', 1),
(28, 'jean 8', 'jean-8', 3344, '42614794_459386674467207_438183498524655616_n-1565246200.jpg', 'kollar', 1),
(29, 'aaaa', 'aaaa', 33, 'p3.jpg', 'kollar', 1),
(30, 'ddaadad', 'dddjdjdd', 55, 'p3.jpg', 'kollar', 1),
(31, 'daaddd', 'dddd', 43, 'p3.jpg', 'viet tien', 1),
(32, 'daaddddhdhhd', 'dhdgga', 3763, 'p3.jpg', 'kollar', 1),
(33, 'product33', 'product-33', 55, 'p4.jpg', 'viet tien', 1),
(34, 'product 34', 'product-34', 366363, 'p4.jpg', 'viet tien', 1),
(35, 'dhdhgdye', 'dhdgge', 73, 'p4.jpg', 'dhdhdh', 1),
(36, 'jean 11', 'jean11', 333, 'p1.jpg', 'kollar', 1),
(37, 'daaddd', 'ettete', 444, 'p3.jpg', 'viet tien', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_attr`
--

CREATE TABLE `product_attr` (
  `product_id` int(11) DEFAULT NULL,
  `atrributes_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_attr`
--

INSERT INTO `product_attr` (`product_id`, `atrributes_id`) VALUES
(1, 1),
(1, 5),
(1, 8),
(4, 2),
(4, 4),
(4, 7),
(8, 1),
(8, 2),
(8, 6),
(8, 8),
(11, 2),
(11, 3),
(11, 7),
(11, 5),
(11, 6),
(11, 8),
(12, 1),
(12, 2),
(12, 4),
(12, 5),
(13, 1),
(13, 2),
(13, 4),
(13, 5),
(14, 2),
(14, 3),
(14, 6),
(14, 8),
(15, 2),
(15, 3),
(15, 5),
(15, 6),
(15, 8),
(16, 3),
(16, 7),
(16, 4),
(16, 5),
(16, 6),
(16, 8),
(17, 1),
(17, 2),
(17, 3),
(17, 5),
(17, 6),
(18, 2),
(18, 6),
(19, 2),
(19, 5),
(19, 6),
(20, 3),
(20, 5),
(20, 6),
(21, 2),
(21, 3),
(21, 5),
(21, 6),
(22, 1),
(22, 3),
(22, 6),
(23, 2),
(23, 3),
(23, 5),
(23, 6),
(24, 2),
(24, 3),
(24, 5),
(24, 6),
(25, 2),
(25, 3),
(25, 4),
(26, 3),
(26, 6),
(27, 2),
(27, 3),
(27, 6),
(27, 8),
(28, 2),
(28, 3),
(28, 6),
(29, 2),
(29, 8),
(30, 7),
(30, 8),
(31, 1),
(31, 2),
(31, 4),
(31, 5),
(31, 5),
(31, 6),
(32, 2),
(32, 3),
(32, 5),
(32, 5),
(32, 6),
(32, 7),
(33, 2),
(33, 5),
(33, 6),
(34, 2),
(34, 3),
(34, 5),
(34, 6),
(35, 1),
(35, 2),
(35, 4),
(35, 5),
(36, 2),
(36, 3),
(36, 5),
(36, 6),
(37, 2),
(37, 3),
(37, 4),
(37, 5);

-- --------------------------------------------------------

--
-- Table structure for table `product_cate`
--

CREATE TABLE `product_cate` (
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_cate`
--

INSERT INTO `product_cate` (`product_id`, `category_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 4),
(2, 5),
(2, 6),
(3, 1),
(33, 5),
(33, 6),
(33, 7),
(34, 4),
(34, 6),
(34, 7),
(35, 5),
(35, 6),
(36, 5),
(36, 6),
(37, 6),
(37, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attr`
--
ALTER TABLE `product_attr`
  ADD KEY `FOREIGN` (`atrributes_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_cate`
--
ALTER TABLE `product_cate`
  ADD KEY `product_id` (`product_id`,`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
