Object.defineProperty(Array.prototype, 'chunk', {
    value: function(chunkSize) {
      var array = this;
      return [].concat.apply([],
        array.map(function(elem, i) {
          return i % chunkSize ? [] : [array.slice(i, i + chunkSize)];
        })
      );
    }
});
function filterData(filterCate)
{
    var action = 'fetch_data';
    var brand = getFilter('brand');
    var color = getFilter('color');
    var size = getFilter('size');
    var categoryFilter = filterCate;


    $.ajax({
        url : 'index.php?controller=pages&action=fetchProduct',
        method : 'POST',
        data : {
            action : action,
            brand : brand,
            categoryFilter : categoryFilter,
            color : color,
            size : size
        },
        success : function(data){
            console.log("data trc khi parse la ", data);
            data = JSON.parse(data);
            if (Object.keys(data).length != 0) {
                var output = '';
                data.chunk(3).forEach(data => {
                    output+='<div class="row row-product">'
                    data.forEach(data => {
                        output+='<div class="col-xs-12 col-sm-4 col-md-4 col-product">'+
                            '<div class="infoProduct text-center">'+
                                '<div class="infoProduct__img">'+
                                    '<a href="">'+
                                        '<img src="public/image/'+data.image+'"' + ' alt="anh san pham">'+
                                    '</a>'+
                                '</div>'+
                                '<div class="infoProduct__name-product">'+ data.name +'</div>'+
    
                                '<div class="infoProduct__price">'+data.price+'</div>'+
                                '<div class="infoProduct__add-to-cart">'+
                                    '<button class="btn">Add to cart</button>'+
                                '</div>'+
                            '</div>'+
                        '</div>'
                    });
                    
                    output+='</div>'
                });
                $('.filter-product').html(output);
            } else {
                $('.filter-product').html('<div class="not-found-product text-center">Sorry no product filter</div>');
            }
        }
    })
};


function getFilter(className)
{
    var filter = [];
    $('.'+className+':checked').each(function() {
        filter.push($(this).val());
    })
    return filter;
};


$('.filter_selector').click(function(){
    var filterCate = [];
    if (typeof($(this).attr('id')) !== undefined) { 
        filterCate.push($(this).attr('id'));
    } else {
        filterCate = [];
    }
    filterData(filterCate);
});
