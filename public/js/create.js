$(function(){
    //validate on client
    $('#create-product-form').validate({
        rules : {
            nameProduct : {
                required : true,
                maxlength: 15
            },
            priceProduct : {
                required : true,
                number: true
            },
            imageProduct : {
                required : true,
                extension : "jpeg|png|gif|jpg"
            },
            brandProduct : "required",
            cateProduct : {
                required : true,
            },
            'colorProduct[]' : {
                required : true
            },
            'sizeProduct[]' : {
                required : true
            },
            status_type : {
                required : true
            },
            slugProduct : {
                required : true
            }
        },
        messages : {
            nameProduct : {
                required : 'Name of product must required',
                maxlength : 'The length of name produtc is not greater than 15'
            },
            priceProduct : {
                required : 'Price product must required',
                number: 'Price must be a number'
            },
            imageProduct : {
                required : 'Image of product must be required',
                extension : 'file must be type jpeg, png or gif'
            },
            brandProduct : {
                required : 'Brand of product must be required',
            },
            'cateProduct' : {
                required : 'Category of product must be required',
            },
            'colorProduct[]' : {
                required : 'Please check color',
            },
            'sizeProduct[]' : {
                required : 'Please check size',
            },
            'status_type' : {
                required : 'Please check status'
            },
            slugProduct : {
                required : 'Slug is not required',
            }
        }
    })

    $('#create-product-form').ajaxForm({
        beforeSubmit: function () {
            return $("#create-product-form").valid();
        },
        success: function (data) {
            console.log(data);
            data = JSON.parse(data);

            if (data.error == true) {
                var output = '';
                output+='<div class="notification-wrap"><ul class="notification-error">'
                Object.keys(data).forEach(function (key){
                    output += '<li>' + data[key] + '</li>'
                });
                output+='</ul></div>';
            } else {
                var output = '';
                output+='<div class="notification-success">'
                Object.keys(data).forEach(function (key){
                    output += '<p>' + data[key] + '</p>'
                });
                output+='</div>';
            }
            $('.notification').html(output);
        },
        clearForm : true
    })

    $('.multi-select-cate').hide();
    $('.multi-select-cate-button').on('click', ()=> {
        $('.multi-select-cate').toggle();
    })  
})
