<?php
    include 'base_controller.php';
    include 'model/base_model.php';
    include 'model/product.php';
    include 'model/category.php';
    include 'model/attributes.php';
    include 'helper/Pagination.php';
    include 'helper/upload.php';

    class PagesController extends BaseController
    {
        function __construct()
        {
            $this->folder = 'pages';
        }

        public function index()
        {
            $product = new Product();
            $category = new Category();
            $attributes = new Attributes();
            $total = $product->getAmountProduct();

            $config = [
                'total' => $total,
                'limit' => 9,
                'full' => false,
                'querystring' => 'page'
            ];
            $pagination = new Pagination($config);
            $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;

            //Data send to view
            $productData = array(
                'productData' => $product->getProduct($currentPage, $config['limit']),
                'menuCateLevel' => $category->getCategory(0),
                'brand' => $product->getBrand(),
                'color' => $attributes->getColor(),
                'size' => $attributes->getSize(),
                'pagination' => $pagination->getPagination()
            );
            $this->render('index', $productData); 
        }

        public function create()
        {
            $category = new Category();
            $attributes = new Attributes();
            $data = array(
                'color' => $attributes->getColor(),
                'size' => $attributes->getSize(),
                'menuCateLevel' => $category->getMultiLevelMenu(0)
            );
            $this->render('create', $data);
        }

        public function store()
        {
            $postData = array();
            $error = array();

            $postData['nameProduct'] = isset($_POST['nameProduct']) ? $_POST['nameProduct'] : '';
            $postData['slugProduct'] = isset($_POST['slugProduct']) ? $_POST['slugProduct'] : '';
            $postData['priceProduct'] = isset($_POST['priceProduct']) ? $_POST['priceProduct'] : '';
            $postData['imageProduct'] = isset($_FILES['imageProduct']['name']) ? getNewNameFile($_FILES['imageProduct']['name']) : ''; 
            $postData['brandProduct'] = isset($_POST['brandProduct']) ? $_POST['brandProduct'] : '';
            $postData['cateProduct'] = isset($_POST['cateProduct']) ? $_POST['cateProduct'] : '';
            $postData['colorProduct'] = isset($_POST['colorProduct']) ? $_POST['colorProduct'] : '';
            $postData['sizeProduct'] = isset($_POST['sizeProduct']) ? $_POST['sizeProduct'] : '';
            $postData['status_type'] = isset($_POST['status_type']) ? $_POST['status_type'] : '';

            //validate on server
            if (empty($postData['nameProduct'])) {
                $error['nameProduct'] = 'Name of product is required';
            }else if (strlen($postData['nameProduct']) > 15) {
                $error['nameProduct'] = 'The length of name produtc is not greater than 15';
            }

            if (empty($postData['priceProduct'])) {
                $error['priceProduct'] = 'Price of product is required';
            }else if (!is_numeric($postData['priceProduct'])) {
                $error['priceProduct'] = 'Price must be a number';
            }

            if (empty($postData['brandProduct'])) {
                $error['brandProduct'] = 'Brand of product is required';
            }

            if (empty($postData['imageProduct'])) {
                $error['imageProduct'] = 'Image of product is required';
            }

            if (empty($postData['cateProduct'])) {
                $error['cateProduct'] = 'Category of product is required';
            }

            if (count($error) > 0) {
                $error['error'] = true;
                echo json_encode($error);
                return;
            } else {
                try {
                    $notification = array();
                    $product = new Product();
                    $product->store($postData);
                    upload_file($_FILES['imageProduct']['name']);
                    $notification['success'] ='your product is added successfully';
                    $notification['error'] = false;
                    echo json_encode($notification);
                } catch(Exception $e) {
                    throw $e;
                    echo 'There seems to be something wrong when you add the product';
                }
            }

        }

        public function fetchProduct()
        {
            $product = new Product();
            $filter = array();
            if(isset($_POST['action'])) {
                if(isset($_POST['brand'])) {
                    $filter['brand'] = $_POST['brand'];
                }
                if (isset($_POST['categoryFilter'])) {
                    $filter['categoryFilter'] = $_POST['categoryFilter'];
                }
                if (isset($_POST['color'])) {
                    $filter['color'] = $_POST['color'];
                }
                if (isset($_POST['size'])) {
                    $filter['size'] = $_POST['size'];
                }
                echo json_encode($product->getFilterAjax($filter));
            }
        }

        public function error()
        {
            $this->render('error');
        }
    }
