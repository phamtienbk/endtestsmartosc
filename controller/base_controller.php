<?php
    class BaseController
    {
        protected $folder;

        function render($file, $data = array())
        {
            $viewFile = 'view/' . $this->folder . '/' . $file . '.php';
            if (is_file($viewFile)) {
                extract($data);
                ob_start();
                require_once($viewFile);
                $content = ob_get_clean();
                include 'view/layout/layout.php';
            } else {
                header('Location: index.php?controller=pages&action=error');
           }
        }
    }
