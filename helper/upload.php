<?php
    /**
     * get new name file
     * 
     * @param string $file
     * @return string $newImageName
     */
    function getNewNameFile($file)
    {
        $randomNum = time();
        $imageName = str_replace(' ','-',strtolower($file));
        $imageExt = substr($imageName, strrpos($imageName, '.'));
        $imageExt = str_replace('.','',$imageExt);
        $imageName = preg_replace("/\.[^.\s]{3,4}$/", "", $imageName);
        $newImageName = $imageName.'-'.$randomNum.'.'.$imageExt;

        return $newImageName;
    }
    /**
     * check type file of image
     * 
     * @param File $path
     * @return boolean
     */
    function checkType($path)
    {
        return getimagesize($path);
    }
    /**
     * upload file
     * 
     * @param $file
     */
    function upload_file($file)
    {
        $pathFile = "public/image/".getNewNameFile($file);
        move_uploaded_file($_FILES['imageProduct']['tmp_name'], $pathFile);
    }

