<?php
    class Category extends BaseModel
    {
        protected $table = 'category';

        public function getMultiLevelMenu($id)
        {
            $html ='';
            $parentMenu = $this->where($this->table, 'parentID', $id);
            foreach ($parentMenu as $row) {
                $html.='<ul>';
                $html.='<label><input type="checkbox" name="cateProduct[]"'.'value ="'.$row['id'].'"' .'class="filter_selector checkbox-inline '.$row['name'].'"'.'>'.$row['name'].'</label>';
                $html.=$this->getMultiLevelMenu($row['id']);
                $html.='</ul>';
            }
            return $html;
        }

        public function getCategory($id)
        {
            $html ='';
            $parentMenu = $this->where($this->table, 'parentID', $id);
            foreach ($parentMenu as $row) {
                $html.='<ul class="multi-menu-category-ul">';
                $html.='<li id="'.$row['id'].'" class="filter_selector">'.$row['name'].'</li>';
                $html.=$this->getCategory($row['id']);
                $html.='</ul>';
            }
            return $html;
        }
    }