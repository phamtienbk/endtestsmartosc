<?php
    class Product extends BaseModel
    {
        protected $table = 'product';

        /**
         * get all product for pagination
         * 
         * @param int $currentPage
         * @param int $limt
         * @return array data
         */
        public function getProduct($currentPage, $limit)
        {
            $start = ($currentPage - 1) * $limit;
            return $this->pagination($this->table, $start, $limit);
        }


        public function store($data)
        {
            $db = DB::getInstance();
            $name = $data['nameProduct'];
            $slug = $data['slugProduct'];
            $price = $data['priceProduct'];
            $cateId = $data['cateProduct'];
            $img = $data['imageProduct'];
            $brand = $data['brandProduct'];
            $status = $data['status_type'];
            $color = $data['colorProduct'];
            $size = $data['sizeProduct'];

            $attributes = array_merge($color, $size);

            $valuesAtrr = array();
            $sql = "INSERT INTO product (name, slug, price, image, brand, status) 
            VALUES ('$name', '$slug', $price, '$img', '$brand', '$status')";
            $sql .= "; SET @product_id = LAST_INSERT_ID()";
            $sql .= "; INSERT INTO product_attr (product_id, atrributes_id) VALUES";

            foreach($attributes as $attrValue) {
                $valuesAtrr[] = "(@product_id, $attrValue)";
            }
            $sql .= implode(', ', $valuesAtrr);

            $valuesCate= array();
            $sql .= "; INSERT INTO product_cate (product_id, category_id) VALUES";
            foreach($cateId as $cateId) {
                $valuesCate[] = "(@product_id, $cateId)";
            }
            $sql .= implode(', ', $valuesCate);
            $stmt = $db->prepare($sql);
            $stmt->execute();
        }

        public function getFilterAjax($filter) {
            $query = "SELECT * FROM product as p WHERE p.status = '1'";
            if (isset($filter['brand'])) {
                $brand_filter = implode("','", $filter['brand']);
                $query.= " AND brand IN('".$brand_filter."')";
            }

            if (isset($filter['categoryFilter'])) {
                $cate_filter = implode("','", $filter['categoryFilter']);
                $query.= " AND p.id IN(SELECT pc.product_id FROM product_cate as pc WHERE pc.category_id IN('".$cate_filter."'))";
            }
            if (isset($filter['color']) && empty(isset($filter['size']))) {
                $color = implode("','", $filter['color']);
                $query.=" AND p.id IN(SELECT pt.product_id FROM product_attr as pt WHERE pt.atrributes_id IN('".$color."'))";
            }
            if(isset($filter['size']) && empty($filter['color'])) {
                $size = implode("','", $filter['size']);
                $query.=" AND p.id IN(SELECT pt.product_id FROM product_attr as pt WHERE pt.atrributes_id IN('".$size."'))";
            }

            if (isset($filter['size']) && isset($filter['color'])) {
                $colorSize = array_merge($filter['size'], $filter['color']);
                $colorSizeFilter = implode("','", $colorSize);
                $query.=" AND p.id IN(SELECT pt.product_id FROM product_attr as pt WHERE pt.atrributes_id IN('".$colorSizeFilter."'))";
            }
            return $this->excuteSql($query);
        }

        public function getBrand() {
            return $this->selectDistinct($this->table, 'brand');
        }

        public function getAmountProduct()
        {
            $db = DB::getInstance();
            $sql = "SELECT COUNT(id) FROM product";
            $result = $db->prepare($sql); 
            $result->execute(); 
            return $result->fetchColumn(); 
        }
    }
