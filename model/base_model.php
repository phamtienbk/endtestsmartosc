<?php
    class BaseModel
    {
        public function excuteSql($sql)
        {
            $db = DB::getInstance();
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            return $result;
        }

        public function getAllItem($table)
        {

            $sql = "SELECT * from $table";
            return $this->excuteSql($sql);
        }

        public function pagination($table, $start, $limit)
        {
            $sql = "SELECT * from $table LIMIT $start, $limit";
            return $this->excuteSql($sql);
        }

        public function where($table, $colum, $value)
        {
            $sql = "SELECT * from $table WHERE $colum = $value";
            return $this->excuteSql($sql);
        }

        public function selectDistinct($table, $colum)
        {
            $sql = "SELECT DISTINCT ($colum) from $table";
            return $this->excuteSql($sql);
        }
    }