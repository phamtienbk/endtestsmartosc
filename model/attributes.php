<?php
    class Attributes extends BaseModel
    {
        protected $table = 'attributes';

        public function getColor() {
            $sql = "SELECT id,value FROM ". $this->table. " WHERE name = 'color'";
            return $this->excuteSql($sql);
        }

        public function getSize()
        {
            $sql = "SELECT id,value FROM ". $this->table. " WHERE name = 'size'";
            return $this->excuteSql($sql);
        }
    }